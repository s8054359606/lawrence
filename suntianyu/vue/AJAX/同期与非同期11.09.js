const getName = () => {
  return new Promise((resolve, reject) => {
    const url = "https://api.github.com/users/deatiger";
    fetch(url)
      .then((res) => res.json())
      .then((json) => {
        console.log("成功");
        return resolve(json.login);
      })
      .catch((error) => {
        console.error("エラー");
        return reject(null);
      });
  });
};
const message = "name is ";
getName().then((username) => {
  console.log(message + username);
});

const getGitUsername = async () => {
  const message = "GitのユーザーIDは";
  const url = "https://api.github.com/users/deatiger";

  const json = await fetch(url)
    .then((res) => {
      console.log("これは非同期処理成功時のメッセージです");
      return res.json();
    })
    .catch((error) => {
      console.error("これは非同期処理失敗時のメッセージです", error);
      return null;
    });
  console.log(message + json.login);
};
getGitUsername();
