// III. オブジェクト自身に変更を加えるメソッド

// 1. 追加と削除 - push() / pop() / shift() / unshift() / splice()
var list = new Array(1, 2, 5)
console.log(list)
console.log(list.length)

var elm1 = list.pop()
console.log(elm1)
console.log(list)

var elm2 = list.push(8)
console.log(`elm2:${elm2}`)
console.log(list)

var elm3 = list.shift()
console.log(`elm3:${elm3}`)
console.log(list)

var elm4 = list.unshift(9)
console.log(`elm4:${elm4}`)
console.log(list)

console.log('-----------------------splice------------------------------------')

list2 = [11, 12, 13, 14, 15];
// インデックス=1から3個の要素を削除し 1 を挿入する。
// splice() メソッドは削除された部分の配列を返す。
var removed = list2.splice(2, 0, 9);
console.log(`removed:${removed}`)
console.log(list2)

// 並べ替え - sort() / reverse()
var list3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// 反転する
list3.reverse();
console.log(list3)
list3.reverse(); // 再び反転すると元に戻る
console.log(list3)

// IV. 配列にアクセスして結果を得るメソッド
// 1. 結合と切り取り - concat() / join() / slice()
console.log('結合と切り取り')

var list = [1, 2, 3];
console.log(list)

// 配列に要素を結合する
var listc = list.concat(4, 5, 6); // [ 1, 2, 3, 4, 5, 6 ]
console.log(list)
console.log(listc)

// 指定要素を間に挟んだ文字列を返す
console.log(list.join(' '))
console.log(list.join('-'))

// 指定した範囲の配列を取得する。
// slice( 開始地点, 終了地点 )
list = [1, 2, 3, 4, 5, 6, 7];
console.log(list)
console.log('list.slice(1):' + list.slice(1))
console.log('list.slice(1,1):' + list.slice(1, 1))
console.log('list.slice(1, 2):' + list.slice(1, 2))
console.log('list.slice(2, 4):' + list.slice(2, 4))
console.log('list.slice(2, -2):' + list.slice(2, -2)) // [3, 4, 5] - 負の数を指定したときは「後ろから何番目」になる


// V. 反復的な操作を行うメソッド
// 1. 反復操作 - forEach()
var list = [1, 2, 3, 4, 5];
var sum = 0;

// 各要素に対して、指定した処理を実行する。
list.forEach(function(elm) {
    sum += elm;
});
console.log('sum:' + sum)

var sum2 = 0
list.forEach(elm => sum2 += elm)
console.log('sum2:' + sum2)


// 2. フィルタリングと関数の適用 - filter() / map()
console.log(' filter() / map()')

var list = [1, 2, 3, 4, 5, 6, 7];
console.log('list:' + list)

// 各要素に対して、指定した条件を満たす要素だけを含む配列を返す。
var result_1 = list.filter(function(element) {
    // elementが2から5の範囲にあるかどうか
    return 2 <= element && element <= 5;
});
console.log('list.filter result_1:' + result_1)

// 各要素に指定した操作を適用した配列を返す。
var result_2 = list.map(function(element) {
    // elementに1を足す
    return element + 1;
});
console.log('list.map result_2:' + result_2)


// 3. チェックテスト - every() / some()
var list = [1, 2, 3, 4, 5, 6, 7];

console.log('★★★★★すべての要素が指定した条件を満たすかどうか★★★★')
    // すべての要素が指定した条件を満たすかどうか調べる。
var result_1 = list.every(function(element) {
    return element < 5;
});
var result_2 = list.every(function(element) {
    return element < 10;
});
console.log('list.every result_1:' + result_1)
console.log('list.every result_2:' + result_2)

console.log('★★★★★1つでも条件を満たす要素が存在するかどうか★★★★')

// 1つでも条件を満たす要素が存在するかどうか調べる。
var result_3 = list.some(function(element) {
    return element == 5;
});
var result_4 = list.some(function(element) {
    return element == 10;
});
console.log('list.some result_3:' + result_3)
console.log('list.some result_4:' + result_4)

console.log('★★★★★総和を求める★★★★')

// 4. 総和を求める - reduce() / reduceRight()
var list = ["abc", "def", "ghi"];
console.log('list:' + list)

// 先頭から順に総和を計算する。
var result_1 = list.reduce(function(sum, element) {
    return sum + element;
});
console.log('list.reduce result_1:' + result_1)


// 後ろから順に総和を計算する。
var result_2 = list.reduceRight(function(sum, element) {
    return sum + element;
});
console.log('list.reduce result_2:' + result_1)

var intList = [10, 20, 70]
console.log(intList.reduce((sum, elm) => sum + elm))

console.log('------------------------------------------------------------------')

// 学生成績の総和、平均
var result = [{
        subject: 'math',
        score: 88
    },
    {
        subject: 'chinese',
        score: 95
    },
    {
        subject: 'english',
        score: 80
    }
];

var sum = result.reduce(function(prev, cur) {
    return cur.score + prev;
}, 0);
console.log('学生成績総和sum＝' + sum)

var avg = result.reduce(function(prev, cur) {
    return (cur.score + prev) / result.length
}, 0);
console.log('学生成績平均avg＝' + avg)

console.log('------------------------------------------------------------------')

console.log('★★★reduceの使い方★★★')
    // reduce中回调函数的参数，这个回调函数有4个参数，意思分别为
    // prev: 第一项的值或上一次叠加的结果值
    // cur: 当前会参与叠加的项
    // index： 当前值的索引
    // arr: 数组本身
var arr = [1, 2, 3, 4, 5];
sum = arr.reduce(function(prev, cur, index, arr) {
    console.log(prev, cur, index);
    return prev + cur;
})
console.log(arr, sum);

// 第二个参数就是设置prev的初始类型和初始值，比如为0
console.log('第二个参数就是设置prev的初始类型和初始值，比如为0');
var arr = [1, 2, 3, 4, 5];
sum = arr.reduce(function(prev, cur, index, arr) {
    console.log(prev, cur, index);
    return prev + cur;
}, 0)
console.log(arr, sum);

// 回调函数
// 一个函数被作为参数传递给另一个函数（在这里我们把另一个函数叫做“otherFunction”），回调函数在otherFunction中被调用。
//首先，创建通用诗的生成函数；它将作为下面的getUserInput函数的回调函数

function genericPoemMaker(name, gender) {
    console.log(name + " is finer than fine wine.");
    console.log("Altruistic and noble for the modern time.");
    console.log("Always admirably adorned with the latest style.");
    console.log("A " + gender + " of unfortunate tragedies who still manages a perpetual smile");
}

//callback，参数的最后一项，将会是我们在上面定义的genericPoemMaker函数
function getUserInput(firstName, lastName, gender, callback) {
    var fullName = firstName + " " + lastName;

    // Make sure the callback is a function
    if (typeof callback === "function") {
        // Execute the callback function and pass the parameters to it
        callback(fullName, gender);
    }
}

// 调用getUserInput函数并将genericPoemMaker函数作为回调函数： 

getUserInput("Michael", "Fassbender", "Man", genericPoemMaker);

// 输出 /* Michael Fassbender is finer than fine wine. Altruistic and noble for the modern time. Always admirably adorned with the latest style. A Man of unfortunate tragedies who still manages a perpetual smile. */


// 计算某元素的个数。
var list = [1, 4, 4, 4, 5];
var cnt = 0;
list.forEach(function(elm) {
    cnt += elm === 4 ? 1 : 0
});
console.log('cnt:' + cnt)