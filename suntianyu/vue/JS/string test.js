var str1 = 'AbcdeFGhIjklmnOPqr'
var str2 = 'Hello World!'

console.log(str1.charAt(5))
console.log(str1.charCodeAt(1))
console.log(str1.concat(str2))
console.log(str1.endsWith('pqr'))
console.log(str2.indexOf('!'))
console.log(str1.repeat(3))