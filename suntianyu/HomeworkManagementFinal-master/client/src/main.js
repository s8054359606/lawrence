import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import axios from "axios";
import VueCookie from "vue-cookie";
import VueAxios from "vue-axios";

import "./element/element.js";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.config.productionTip = false;
Vue.use(VueCookie);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

// router.beforeEach((to, from, next) => {
//   if (to.meta.title) {
//     document.title = to.meta.title;
//   }
//   next();
// });

router.beforeEach(function(to, from, next) {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  var array = ["/", "/register", "/login", "/logout"];
  if (array.includes(to.path)) {
    next(); //继续往下执行，跳转到to所指定的页面
  } else {
    //无登录状态,跳转到login页面
    let token = Vue.prototype.$cookie.get("token");
    let teacher = Vue.prototype.$cookie.get("teacher");
    if (token) {
      //student已经登录的场合
      if (JSON.parse(teacher) == false && to.path == "/student") {
        next(); //继续往下执行，跳转到to所指定的页面
      } //teacher已经登录的场合
      else if (
        (JSON.parse(teacher) == true && to.path == "/teacher") ||
        to.path == "/teacher/add_homework" ||
        to.path == "/teacher/homework_list"
      ) {
        next(); //继续往下执行，跳转到to所指定的页面
      } //无登录的场合
      else {
        next({
          path: "/", //无登录状态,跳转到home页面（login画面）
        });
      }
    } else {
      next({
        path: "/", //无登录状态,跳转到home页面（login画面）
      });
    }
  }
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
