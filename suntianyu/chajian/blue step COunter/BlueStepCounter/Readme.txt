-------------------------------------------------------------------------------
ステップカウンタ「Blue Step Counter」
                                                             Since: 2003/08/01
                                                                Version: 1.4.0
                                                      Author: Yoshihiro Aoyama
                                                       Last Change: 2008/02/04

                          Copyright (C) 2003-2008  BlueCARD, Yoshihiro Aoyama.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
■Blue Step Counterとは
　　Blue Step Counterとは各種プログラムソースファイルの実行行・コメント行・空白
　行のステップ数をカウントするソフトウェアです。

○独自コメントキーワードに対応可能
　　特殊エンジンの設定を行うことによりプログラムソースファイルのみでなく、独自
　のコメントキーワードを持つテキストファイルのステップカウントにも対応していま
　す。

○Microsoft Visual Studio関連のプロジェクトファイルに対応
　　Microsoft Visual Studio関連のプロジェクトファイルにおいてはプロジェクト
　ファイルを指定することにより、登録されている全てのファイル（外部依存関係の
　ファイルは対象外）をステップカウントの対象とすることができます。
　　以下のものが対応プロジェクトファイルです。
　　　・Visual C++ ワークスペース　　　　　　　　　　　　(.dsw)
　　　・Visual C++ プロジェクト　　　　　　　　　　　　　(.dsp)
　　　・Visual Basic プロジェクト　　　　　　　　　　　　(.vbp)
　　　・Microsoft Visual Studio .NET Solution File　　　 (.sln)
　　　・Visual C++ .NET プロジェクト　　　　　　　　　　 (.vcproj)
　　　・Visual C# .NET プロジェクト 　　　　　　　　　　 (.csproj)
　　　・Visual Basic .NET プロジェクト　　　　　　　　　 (.vbproj)
　　　・Microsoft Visual Studio .NET 2003 Solution File　(.sln)
　　　・Visual C++ .NET 2003 プロジェクト　　　　　　　　(.vcproj)
　　　・Visual C# .NET 2003 プロジェクト 　　　　　　　　(.csproj)
　　　・Visual Basic .NET 2003 プロジェクト　　　　　　　(.vbproj)
　　　・Microsoft Visual Studio 2005 Solution File　　　 (.sln)    (*1)
　　　・Visual C++ 2005 プロジェクト 　　　　　　　　　　(.vcproj) (*1)
　　　・Visual Basic 2005 プロジェクト 　　　　　　　　　(.vbproj) (*1)
　　　・Visual C# 2005 プロジェクト  　　　　　　　　　　(.csproj) (*1)
　　　・Microsoft Visual Studio 2008 Solution File　　　 (.sln)    (*1)
　　　・Visual C++ 2008 プロジェクト 　　　　　　　　　　(.vcproj) (*1)
　　　・Visual Basic 2008 プロジェクト 　　　　　　　　　(.vbproj) (*1)
　　　・Visual C# 2008 プロジェクト  　　　　　　　　　　(.csproj) (*1)
　　　・eMbedded Visual C++ ワークスペース　　　　　　　 (.vcw)
　　　・eMbedded Visual C++ プロジェクト　　　　　　　　 (.vcp)
　　　・eMbedded Visual Basic プロジェクト　　　　　　　 (.ebp)

　　　　(*1) Express Editionにて動作確認

○ディレクトリ指定に対応（サブディレクトリ含む）
　　ディレクトリの指定も可能となっており、ディレクトリを指定した際は、下位の
　ディレクトリも再帰的にステップカウント対象のファイルを検索しステップカウント
　を行います。

○拡張子に対する解析エンジンの指定が可能
　　『コメント書式はC++だけどファイル拡張子がcppではない』という場合には、環境
　設定の「カウント対象ファイル」にそのファイルの拡張子を追加し、解析エンジンを
　「C/C++ File Engine」とすることにより拡張子を変更しなくてもステップカウントが
　行えるようになります。

○カウント結果がファイルで保存可能
　　カウント結果がプロジェクトファイルとして保存可能となっています。

○前回カウント結果とのステップ数の差分算出が可能
　　プロジェクトファイルと現在のカウント結果との比較を行い、ステップ数の差分を
　算出することが可能となっています。

-------------------------------------------------------------------------------
■動作環境
　【対応ＯＳ】
　　以下のＯＳにて使用可能です。
　　ただし、完全な動作を保証するものではありません。

　　　・Windows 98 SE
　　　・Windows NT4.0
　　　・Windows ME
　　　・Windows 2000
　　　・Windows XP

　　　※　Internet Explorer 5.0以上がインストールされている必要があります。

　【マシン】
　　対応ＯＳが動作するマシン。

　【メモリ】
　　対応ＯＳが動作するのに必要なメモリ容量。

-------------------------------------------------------------------------------
■インストール
　　インストーラーはありませんので、Blue Step Counterのファイルを任意のフォルダ
　に置いてください。

-------------------------------------------------------------------------------
■アンインストール
　　アンインストーラーはありませんので、Blue Step Counterのファイルを任意のフォ
　ルダごと削除してください。
　　なお、レジストリは使用していませんので、レジストリを削除する必要はありません。

-------------------------------------------------------------------------------
■操作方法
　　付属のヘルプを参照してください。

-------------------------------------------------------------------------------
■著作権および免責
　　このソフトはフリーソフトウェアです。
　　著作権は作者である、BlueCARD( Yoshihiro Aoyama )にあります。
　　このプログラムの使用により生じたいかなる損害に対しても、作者は一切責任を負
　いません。利用者の責任で使用して下さい。
　　転載は基本的には自由ですが、改変は行わず元のアーカイブのままにして下さい。
　転載の際には作者への報告をお願いします。

　　Windows98 SE/NT4.0/ME/2000/XPは、Microsoft社の商標です。
　　Internet Explorerは、Microsoft社の商標です。
　　Visual Studioは、Microsoft社の商標です。
　　Visual C++は、Microsoft社の商標です。
　　Visual Basicは、Microsoft社の商標です。
　　Visual C++ .NETは、Microsoft社の商標です。
　　Visual Basic .NETは、Microsoft社の商標です。
　　Visual C# .NETは、Microsoft社の商標です。
　　Visual C++ .NET 2003は、Microsoft社の商標です。
　　Visual Basic .NET 2003は、Microsoft社の商標です。
　　Visual C# .NET 2003は、Microsoft社の商標です。
　　Visual C++ 2005は、Microsoft社の商標です。
　　Visual Basic 2005は、Microsoft社の商標です。
　　Visual C# 2005は、Microsoft社の商標です。
　　Visual C++ 2008は、Microsoft社の商標です。
　　Visual Basic 2008は、Microsoft社の商標です。
　　Visual C# 2008は、Microsoft社の商標です。
　　eMbedded Visual C++は、Microsoft社の商標です。
　　eMbedded Visual Basicは、Microsoft社の商標です。

-------------------------------------------------------------------------------
■ホームページおよび連絡先
　【ホームページ】
    http://bluecard.no-ip.com/freeware/bluestepcounter/

　【連絡先】
　　BlueStepCounter@bluecard.no-ip.com
　　　または
　　BlueCARD@bluecard.no-ip.com

-------------------------------------------------------------------------------
■変更履歴
  ▼2008/02/04 Version 1.4.0
    ・Visual Studio .NET 2003、2005、2008のプロジェクトファイル読み込みに対応
    　（宇津川様、島村様、ご要望ありがとうございました！）
    ・UTF-8 BOMありのファイルをカウントすると１行目が必ず実行行になるバグを修正
    　（原嶋様、ご指摘ありがとうございました！）
    　なお、上記修正を含め以下の文字コードのファイルでのカウントに対応
    　　・JIS
    　　・Shift-JIS
    　　・EUC
    　　・UTF-8 BOMなし
    　　・UTF-8 BOMあり
    　　・UTF-16リトルエンディアン BOMあり
    　　・UTF-16ビッグエンディアン BOMあり
    ・ショートカットアイコンへの複数ファイルのドロップに対応
    ・カウントリストにエンコード種別のカラムを追加
    　（注）旧バージョンの環境情報がある場合には、環境設定にて表示項目リストの
    　　　　変更をすることによりエンコード種別が表示されます。

  ▼2006/09/19 Version 1.3.4
    ・メニュー「設定」−「カウント対象ファイル」にて切り替えを行った際に、選択
    　合計が正しく表示更新されないバグを修正
    ・ステップカウント処理のメモリリークを修正
      （ujima様、ご指摘ありがとうございました！）

  ▼2006/07/06 Version 1.3.3
    ・環境情報ファイルの旧バージョンからの更新時に特殊エンジンの設定が正しく
    　更新されないバグを修正
    　（注）Version 1.3.2で特殊エンジンの設定が正しく更新されなかった方はお手数
    　　　　をお掛けしますが、BStepCnt.iniを削除してからバックアップファイル
    　　　　(BStepCnt.bak)をBStepCnt.iniに名称変更し当バージョンを実行して
    　　　　ください。

  ▼2006/07/01 Version 1.3.2
    ・「エンジン」の名称を全て「ルール」に変更
    ・環境設定の特殊エンジン設定を廃止し、解析ルールの機能を追加
    ・環境設定の特殊エンジンを廃止し、同様の内容を解析ルールに追加できる機能を
    　追加
    ・ポップアップメニューに「ファイルを開く｣を追加
      （マッキー様、ご要望ありがとうございました！）
    ・ポップアップメニューに「フォルダを開く｣を追加
      （マッキー様、ご要望ありがとうございました！）
    ・eMbedded Visual C++ のプロジェクトファイルの読み込みに対応
    ・eMbedded Visual Basic のプロジェクトファイルの読み込みに対応
    ・環境情報ファイルの調整
    　（旧バージョンの環境情報ファイルがある場合は、初回起動時に自動更新され
    　　ます。）
    ・ヘルプファイルの更新

    ・「タブ区切りテキスト形式で保存」と「CSV形式で保存」において、ファイルの
    　上書き確認をキャンセルしても上書きされてしまうバグを修正
    ・「プロジェクトからの差軍計算を開始」実行時に選択合計の表示が更新されない
    　バグを修正
    ・選択行がカウント対象ファイルから削除された時に、選択合計の表示が更新され
    　ないバグを修正

  ▼2006/04/03 Version 1.3.1
    ・ツールバーのボタン配列のバグを修正

  ▼2006/03/30 Version 1.3.0
    ・ファイルサイズおよび更新日時の表示を追加
    　（河合様、ご要望ありがとうございました！）
    ・Blue Step Counterプロジェクトファイルのフォーマットを変更
    　（旧バージョンの読み込みも可能）
    ・環境設定にリスト表示設定の機能を追加
    ・選択合計の機能を追加
    ・ウィンドウを閉じる際に保存確認メッセージの表示を追加
    ・特殊コメントのコメントワード入力文字数を60文字に拡張
    ・カウントリストに操作のポップアップメニュー表示を追加
    ・メニュー項目の操作可／不可のタイミングおよび表記を調整
    ・メニューに「選択行をクリップボードにコピー」を追加
    ・メニューに「選択行をカウント」を追加
    ・印刷処理の見直し
    ・ヘルプファイルの更新

  ▼2005/05/08 Version 1.2.2
    ・Microsoft Visual Studio関連のプロジェクトファイルを指定した際に、その
    　プロジェクトに登録されているファイルがサブフォルダを含んでいる場合に、
    　「ファイルが存在しない」と判断されてしまうバグを修正

  ▼2004/12/07 Version 1.2.1
    ・Visual C#.NETのプロジェクトファイルが「ファイル」−「ファイルを開く」で
    　選択できないバグを修正
      （徳山様、ご指摘ありがとうございました！）

  ▼2004/07/21 Version 1.2.0
    ・解析エンジンに以下のエンジンを追加
    　　「VB.NET File Engine」
    　　「Perl File Engine」
    　　「SQL File Engine」
    ・初期状態のカウント対象ファイルに以下のファイルを追加
    　　"cs"ファイル 　　「C/C++ File Engine」
    　　"vb"ファイル 　　「VB.NET File Engine」
    　　"pl"ファイル 　　「Perl File Engine」
    　　"cgi"ファイル　　「Perl File Engine」
    　　"sql"ファイル　　「SQL File Engine」
      ※すでにINIファイルが存在している場合は、カウント対象リストに上記の拡張子
      　が自動では追加されませんので、手動で追加する必要があります。
    ・Visual C++.NETのプロジェクトファイル（vcprojファイル）の指定により対象
    　ファイルを取り込める機能を追加
    　（Sota様、ご要望ありがとうございました！）
    ・上記に伴い、初期状態のステータスバーメッセージ「〜を指定してください」を
    　変更
    ・Visual Basic.NETのプロジェクトファイル（vbprojファイル）の指定により対象
    　ファイルを取り込める機能を追加
    ・Visual C#.NETのプロジェクトファイル（csprojファイル）の指定により対象
    　ファイルを取り込める機能を追加
    ・Microsoft Visual Studio Solution File（slnファイル）の指定により対象
    　ファイルを取り込める機能を追加
    ・Blue Step Counterプロジェクトファイルを開く動作が情報の追加になっていたが
    　指定のプロジェクトファイルの情報のみを開く様に仕様変更し、追加機能は
    　「プロジェクトを追加」をメニューを追加することで対応した
      （Pires様、ご要望ありがとうございました！）
    ・「設定」−「カウント対象ファイル」にて切り替えを行うとソート状態がおかしく
    　なるバグを修正
      （Sota様、ご指摘ありがとうございました！）
    ・以下の処理を最適化
　　　　・INIファイル関連
　　　　・環境設定関連
　　　　・解析エンジン関連
　　　　・プロジェクトファイル解析関連
    ・上記の各修正に伴い、ヘルプを修正

  ▼2004/02/16 Version 1.1.2
    ・COBOL File Engineのソース行開始位置の判定を変更
      　8カラム目からの"PROCEDURE DIVISION"と完全一致
      　　　→　8カラム目からの"PROCEDURE"と完全一致
      （舟木様、ご助言ありがとうございました！）
    ・上記に伴い、ヘルプを修正

  ▼2004/01/26 Version 1.1.1
    ・Blue Step Counterプロジェクトファイルを指定したリストに、他のプロジェクト
    　ファイルを追加指定すると一時設定の内容が後から指定したプロジェクトファイル
    　のものになってしまい、リストの表示がおかしくなるバグを修正
    　（一時設定の内容は先に指定したプロジェクトファイルのを使用するよう修正）
    ・合計リストの横スクロールバーが消えることがあるバグを修正

  ▼2003/11/25 Version 1.1.0
    ・作業中のカウントリストの内容をクリップボードにコピーする機能を追加
    　それに伴い、メニュー「ファイル」に「クリップボードにコピー(L)」を追加
    ・作業中のカウントリストの内容をタブ区切りのテキスト形式で保存する機能を追加
    　それに伴い、メニュー「ファイル」に「タブ区切りテキスト形式で保存(T)...」
    　を追加
    ・環境設定「特殊エンジン」にブロックコメントワード２の設定を追加
      （川端様、上記３件のご要望ありがとうございました！）
      （Sota様、「クリップボードにコピー」のご要望ありがとうございました！）

    ・ステータスバーにカウント日付の表示を追加
    ・印刷結果にカウント日付の出力を追加
    ・Blue Step Counterプロジェクトファイルのフォーマットを変更
    　カウント日付と「メニュー」−「設定」の一時設定を保存するように修正
    　（旧バージョンの読み込みも可能）
    ・環境設定「特殊エンジン」の表示文字を変更
    　　コメントブロックワード　→　ブロックコメントワード１
    ・「コメントブロックワード」に関するINIファイルのキー名称を変更
      （旧バージョンのINIファイルがある場合は起動時に自動変換します）
    ・ツールバーのアイコンを操作不可能状態（グレー表示）でも見やすいように調整
    ・カウントリストと合計リストのカラムタイトルを変更
    　　実行ステップ比　→　実行比
    ・以下のタイミングで行っていた再カウントを止め、表示更新に変更
    　　・「メニュー」−「操作」−「プロジェクトからの差分計算を終了」時
    　　・「ツールバー」−「プロジェクトからの差分計算を終了」時
    　　・「メニュー」−「設定」−「カウント対象ファイル」の切り替え時
    　　・「メニュー」−「設定」−「空白行のカウント方法」の切り替え時
    ・上記の各修正に伴い、ヘルプを修正

  ▼2003/10/15 Version 1.0.7
    ・Visual C++のプロジェクトワークスペース(dsw)を解析してカウント対象ファイル
      を抽出する機能を追加

    ・Visual C++のプロジェクトファイル(dsp)からカウント対象ファイルを抽出する際
      にプロジェクトファイル(dsp)上で絶対パスのファイルが不正に認識されている
      バグを修正
    ・BlueStepCounterプロジェクトファイルを同じカウントリスト上で２回以上読み
      込む（または、ファイルドロップをする）と同じファイルが複数表示されてしま
      うバグを修正
    ・ヘルプファイルが時々表示されないバグを修正
    ・AAA.BBB.cpp等、ファイル名称に'.'がある場合に最初の'.'以降をカウント対象の
      拡張子としていたバグを修正

  ▼2003/10/02 Version 1.0.6
    ・メインツールバーの「〜を開く」のアイコンを調整
    ・カウント中ダイアログの進捗表示を調整
    ・ソースカウントの処理時間を短縮するように修正（高速化対応）

  ▼2003/09/15 Version 1.0.5
    ・環境設定「特殊エンジン」に大文字小文字を区別するかの設定を追加
    ・メニュー「設定」の「空白行を総ステップ数に含む」を「空白行のカウント方法」
      に変更し、空白行のカウント方法を選択できる機能を追加
    ・環境設定「カウント方法」の「空白行を総ステップ数に含む」を「空白行のカウ
      ント方法」に変更し、空白行のカウント方法のデフォルトを選択できる機能を追加

    ・自動カウントが有効な状態で、BlueStepCounterプロジェクトを開いているカウン
      トリストにカウント対象ファイルを追加しても自動カウントされないバグを修正
    ・BlueStepCounterプロジェクトを「上書き保存」する際に上書き確認のダイアログ
      が表示されるバグを修正

  ▼2003/09/04 Version 1.0.4
    ・特殊エンジンのコメント・ブロックコメント判断が正しく行われないバグを修正

  ▼2003/09/03 Version 1.0.3
    ・「カウント対象ファイルの追加」ダイアログで拡張子入力後に自動的に拡張子
      名称が設定される機能を冗長機能と判断し削除
      それに伴い、拡張子名称入力エリアの下に「※ 省略するとデフォルト名称を使用
      する」のメッセージ表示を追加
    ・解析エンジンに「COBOL File Engine」を追加
      すでにINIファイルが存在している場合は、カウント対象リストに「cbl」拡張子
      が自動では追加されませんので、手動で追加する必要があります。
    ・上記の各修正に伴い、ヘルプファイルを修正

    ・ブロックコメント内の空白行を空白行でカウントしていたバグを修正

  ▼2003/09/01 Version 1.0.2
    ・動作環境に「要Internet Explorer5.0」の記述を追加
    ・ファイルを追加する際のファイル追加／カウントのタイミングを調整
    ・処理中ダイアログを登録中ダイアログに変更
    ・分散していた解析エンジンの処理を統合化
    ・メニュー「操作」−「カウント」のショートカットキーを変更（(R)→(C)）
    ・メニュー「設定」に「自動カウント」を追加
    ・ツールバーに「自動カウント」を追加
      カウントで使用していたアイコンを自動カウントで使用し、カウントのアイコン
      を新規作成
    ・環境設定「カウント方法」の「カウント対象〜(I)」チェックボックスの名称を
      「自動カウント(U)」に変更
    ・「自動カウント」に関するINIファイルのキー名称を変更
      （旧バージョンのINIファイルがある場合は起動時に自動変換します）
    ・上記の各修正に伴い、ヘルプファイルを修正

    ・最終行が改行でなくEOFの場合にカウント数が１行足りなくなるバグを修正
      （ナオーバ様、ご指摘ありがとうございました！）

  ▼2003/08/20 Version 1.0.1
    ・以下の場合の処理中ダイアログの表示を調整
    　　・ディスクトップ上のアイコンにファイルをドロップした時の単独表示を抑止
    　　・ウィンドウに複数ファイルをドロップした時のちらつきを抑止
    　　・ファイル選択ダイアログから複数ファイルを選択した時のちらつきを抑止

  ▼2003/08/01 Version 1.0.0
    ・リリース開始

-------------------------------------------------------------------------------
