/*
 * Copyright 2005 TSS Tsuta
 */
package tss.sc.counter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JTable;

import tss.sc.gui.table.MyTableModel;
import tss.sc.gui.util.Reporter;
import tss.sc.util.Constants;

/**
 * @author tsuta
 * 
 * カウンターの基底クラス
 */
public abstract class Counter {

	protected static final int STATEMENT_LINE = 0;

	protected static final int COMMENT_LINE = 1;

	protected static final int BLANK_LINE = 2;

	private static final String ENCODING = "eucjp";

	private static final int BUFFER_SIZE = 4096;

	private static final String PROP_FILE_PATH = "./counter.properties";

	private static Properties prop = null;

	private static HashMap concreteClassMap = new HashMap();

	static {
		Properties prop = new Properties();
		File file = new File(PROP_FILE_PATH);
		if (file.isFile()) {
			try {
				prop.load(new FileInputStream(file));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		Enumeration enum = prop.propertyNames();
		String key;
		String fqcn;
		while (enum.hasMoreElements()) {
			key = (String) enum.nextElement();
			try {
				fqcn = prop.getProperty(key);
				concreteClassMap.put(key, Class.forName(fqcn).newInstance());
			} catch (Exception e) {
				e.printStackTrace();
				Reporter.report(Constants.REPORT_ERROR, e);
			}
		}
	}

	/**
	 * 指定の行に指定ファイルのカウントを反映する
	 * 
	 * @param table
	 * @param rowIndex
	 * @param file
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void countAndReflect(JTable table, int rowIndex, File file)
			throws UnsupportedEncodingException, FileNotFoundException,
			IOException {

		MyTableModel tableModel = (MyTableModel) table.getModel();
		int[] counts = count(file);
		if (counts != null) {
			tableModel.setValueAt(new Integer(counts[0]), rowIndex, 3);
			tableModel.setValueAt(new Integer(counts[1]), rowIndex, 4);
			tableModel.setValueAt(new Integer(counts[2]), rowIndex, 5);
			tableModel.setValueAt(new Integer(counts[3]), rowIndex, 6);
		}
	}

	/**
	 * 指定のファイルのステートメント行、コメント行、空行および全行数をカウントし 
	 * この並びでintの配列として返す。 <br>
	 * 指定のファイルをカウントするためのカウンターが存在しない場合はnullを返す。
	 * 
	 * @param file
	 * @return int[] カウントの配列
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static int[] count(File file) throws UnsupportedEncodingException,
			FileNotFoundException, IOException {

		Counter counter = getConcreteClass(file.getName());
		if (counter == null) {
			return null;
		}

		BufferedReader reader = null;
		int stmtCount = 0;
		int commentCount = 0;
		int blankCount = 0;
		int allCount = 0;

		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(file.getPath()), ENCODING), BUFFER_SIZE);
			counter.init();

			String line;
			int result;
			while ((line = reader.readLine()) != null) {
				allCount++;
				result = counter.distinguishLine(line);
				if (result == STATEMENT_LINE) {
					stmtCount++;
				}
				if (result == COMMENT_LINE) {
					commentCount++;
				}
				if (result == BLANK_LINE) {
					blankCount++;
				}
			}

			counter.finish();

		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		return new int[] { stmtCount, commentCount, blankCount, allCount };
	}

	public static String deleteSpace(String str) {
		str = str.replaceAll(" ", "");
		str = str.replaceAll("\t", "");
		return str;
	}

	//---------------------------------------------------------- private
	/**
	 * ファイルの拡張子にマッピングされたCounterの具形クラスを返す。 
	 * 該当するクラスが無い場合はnullを返す。
	 */
	private static Counter getConcreteClass(String fileName) {

		Counter counter = null;
		int lastIndex = fileName.lastIndexOf('.');
		if (lastIndex != -1) {
			String extension = fileName.substring(lastIndex + 1, fileName
					.length());
			counter = (Counter) concreteClassMap.get(extension);
		}

		return counter;
	}

	//------------------------------------------------------- template methods

	/**
	 * 新たなファイルをカウントする前に1度だけ実行される。 
	 * 必要に応じてカウント開始前の初期化処理を実装する。
	 */
	protected void init() {
	}

	/**
	 * ファイルをカウントし終えたときに1度だけ実行される。 
	 * 必要に応じてカウントを終えたときの後始末処理を実装する。
	 */
	protected void finish() {
	}

	//---------------------------------------------------------- abstract

	/**
	 * 引数で得た行が、実行行かコメント行か空行かを判定し判定値を返す。
	 * 
	 * @param line
	 * @return 実行行の場合はCounter.STATEMENT_LINEを <br>
	 *         コメント行の場合はCounter.COMMENT_LINEを <br>
	 *         空行の場合はCounter.BLANK_LINEをを返す
	 */
	abstract protected int distinguishLine(String line);
}

