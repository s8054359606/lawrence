/*
* Copyright 2005 TSS Tsuta
*/
package tss.sc.counter;


/**
 * @author tsuta
 *
 * C言語用カウンター
 */
public class CLangCounter extends Counter {

    boolean isBlockComment = false;

	protected void init() {
	    this.isBlockComment = false;
	}

    
	protected int distinguishLine(String line) {

		int lineType;
		boolean isBlockeCommentStart = false;
		boolean isBlockeCommentEnd   = false;
		line = line.trim();
		
		if (line.length() > 0) {

		    if ((!isBlockComment) && (line.indexOf("/*") != -1)) {
		        String[] strs = line.split("\"");
		        for (int i = 0; i < strs.length; i++) {
		            if ((i % 2 == 0) && (strs[i].indexOf("/*") != -1)) {
		                isBlockeCommentStart = true;
		                isBlockComment = true;
		            }
		        }
		    }

		    if ((isBlockComment) && (line.indexOf("*/") != -1)) {
		        String[] strs = line.split("\"");
		        for (int i = 0; i < strs.length; i++) {
		            if ((i % 2 == 0) && (strs[i].indexOf("*/") != -1)) {
		                isBlockeCommentEnd = true;
		            }
		        }
		    }


		    if (isBlockComment) {

		        if (isBlockeCommentStart && !isBlockeCommentEnd) {
			        if (line.startsWith("/*")) {
			            lineType = Counter.COMMENT_LINE;
			        } else {
			            lineType = Counter.STATEMENT_LINE;
			        }
			        
		        } else if (!isBlockeCommentStart && isBlockeCommentEnd) {
			        if (line.endsWith("*/")) {
			            lineType = Counter.COMMENT_LINE;
			        } else {
			            lineType = Counter.STATEMENT_LINE;
			        }

			        isBlockComment = false;

		        } else if (isBlockeCommentEnd && isBlockeCommentEnd) {
			        if (line.startsWith("/*") && line.endsWith("*/")) {
			            lineType = Counter.COMMENT_LINE;
			        } else {
			            lineType = Counter.STATEMENT_LINE;
			        }

			        isBlockComment = false;
			        
		        } else {
		            lineType = Counter.COMMENT_LINE;		            
		        }
		        
		    } else if (line.startsWith("//")) {
		        lineType = Counter.COMMENT_LINE;

		    } else {
		        lineType = Counter.STATEMENT_LINE;
			}
		} else {
		    lineType = Counter.BLANK_LINE;
		}
		
		return lineType;
	}
}
