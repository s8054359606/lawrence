/*
* Copyright 2005 TSS Tsuta
*/
package tss.sc.counter;


/**
 * @author tsuta
 *
 * シェル用カウンター
 */
public class ShellCounter extends Counter {

    boolean isFirst = true;

	protected void init() {
	    this.isFirst = true;
	}

    
	protected int distinguishLine(String line) {

		int lineType;
		line = deleteSpace(line);
		
		if (line.length() > 0) {
		    if (line.startsWith("#!") && isFirst) {
		        lineType = Counter.STATEMENT_LINE;

		    } else if (line.charAt(0) == '#') {
		        lineType = Counter.COMMENT_LINE;

		    } else {
		        lineType = Counter.STATEMENT_LINE;
			}
		} else {
		    lineType = Counter.BLANK_LINE;
		}
		
		if (isFirst) {
		    isFirst = false;
		}
		
		return lineType;
	}
}
