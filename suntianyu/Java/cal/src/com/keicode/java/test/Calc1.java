package com.keicode.java.test;

import javax.jws.*;

@WebService
public class Calc1 {

     public double add( 
          @WebParam(name="x") double x, 
          @WebParam(name="y") double y 
          ) {
     
          return x + y;
     
     }     
     
}
