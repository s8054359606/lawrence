package com.keicode.java.test;

import javax.xml.ws.*;

public class Calc1Server {

     public static void main(String[] args) {

          Endpoint.publish( 
               "http://localhost:8888/WebServices/calc1", 
               new Calc1() );
          
     }
     
}