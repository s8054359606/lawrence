package com.site.springboot.core.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//JDK8匿名内部类写法
new Thread(
		() -> {
			System.out.print("Hello");
			System.out.println("Hoolee");
		}
		).start();


public class Lambda {

List<String> list = Arrarys.asList("I","Love","you","too");
Collections.sort(list,(s1,s2) ->{
	if(s1 == null)
		return -1;
	if(s2 == null)
		return 1;
	return s1.length()-s2.length();
});

Runnable run = () -> System.out.println("Hello World");
ActionListener listener = event -> System.out.println("button clicked");
Runnable multiLine = () -> {
	System.out.print("Hello");
	System.out.println(" Hoolee");
};
BinaryOperator<Long> add = (Long x, Long y) -> x + y;
BinaryOperator<Long> addImplicit = (x, y) -> x + y;
};

public class MainLambda {
	public static void main(String[] args) {
		new Thread(
				() -> System.out.println("Lambda Thread run()")
				).start();;
	}
public class Hello {
	Runnable r1 = () -> { System.out.println(this);};
	Runnable r2 = () -> { System.out.println(toString());};
	public static void main(String[] args) {
		new Hello().r1.run();
		new Hello().r2.run();
	}
	
	public String toString() { return "Hello Hoolee";}
}
ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
for(String str : list) {
	if(str,length()>3)
		System.out.println(str);
}

ArrayList<String>list = new ArrayList<>(Arrays.asList("I","love","you","too"));
list.foreach(new Consumer<String>() {
	@Override
	public void accept(String str) {
		if(str.length()>3)
			System.out.println(str);
	}
});

ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
Iterator<String> it = list.interator();
while(it.hasNext()) {
	if(it.next().length()>3)
		it.remove();
}
ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
list.removeIf(new Predicate<String>() {
	@Override
	public boolean test(String str) {
		return str.lenth()>3;
	}
});

ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
for(int i=0);i<list.size(); i++){
	String str = list.get(i);
	if(str.length()>3)
		list.set(i,str.toUpperCase());
}

ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
list.replaceAll(str -> {
	if(str.length()>3)
		return str.toUpperCase();
	return str;
});

ArrayList<String> list = new ArrayList<>(Arrays.asList("I","love","you","too"));
Collections.sort(list, new Comparator<String>() {
	@Override
	public int compare(String str2,String str2) {
		return str1.length()-str2.length();
		
	}
});
Stream<String> stream= Stream.of("I","love","you","too");
stream.filter(str -> str.length()==3)
	.forEach(str -> System.out.println(str));

Stream<String> stream = Stream.of("I","love", "you", "too");
stream.map(str -> str.toUpperCase())
	.forEach(str -> System.out.println(str));


Stream<List<Integer>> stream =
Stream.of(Arrays.asList(1,2), Arrays.asList(3,4,5));
stream.flatMap(list -> list.stream())
.forEach(i -> System.out.println(i));

Stream<String> stream = Stream.of("I","love","you","too");
Optional<String> ongest = stream.reduce((s1,s2)) ->
s1.length()>=s2.length() ? s1 : s2);
System.out.println(longest.get());


ArrayList<String> list = new ArrayList<>(Arrays.asList(""))

ArrayList<String> arrayList =
stream.collet(Collectors.toCollection(ArrayList::new));
HashSet<String> hasSet =
stream.collect(Collectors.toCollection(HashSet::new));

if (map.get(key) != null) {
	v oldValue = map.get(key);
	V newValue = remappingFunction.apply(key,oldValue);
	if (newValue != null)
		map.put(key,newValue);
	else
		map.remove(key);
	return newValue;
}
return null;


HashMap<Integer,String> map = new HashMap<>();
map.put(1,"one");
map.put(2,"two");
map.put(3,"three");
map.replaceAll(new BiFunction<Integer, String, String>(){
	@Override
	public String apply(Integer k,String v) {
		return v.toUpperCase();
	}
});

HashMap<Integer,String> map = new HashMap<>();
map.put(1,"one");
map.put(2,"two");
map.put(3,"three");
map.replaceAll((k,v) -> v.toUpperCase());

Map<Integer,Set<String>> map = new HashiMap<>();
if(map.containskey(1)) {
	map.get(1).add("one");
}else {
	set<String> valueSet = new HashiSet<String>();
	valueSet.add("one");
	map.put(1,valueSet);
}
map.computeIfAbsent(1, v -> new HashiSet<String>()).add("yi");

if(map.get(key) !=null) {
	V oldValue = map.get(key);
	V newValue = remappingFunction.apply(key, oldValue);
	if (newValue != null)
		map.put(key, newValue);
	else
		map.remove(key);
	return newValue;
}
return null;

Stream<String> stream= Stream.of("I","love", "you", "too");
stream.filter(str -> str.length()==3)
	.forEach(str -> System.out.println(str));

Stream<String> stream = Stream.of("I","love","you","too");
List<String> list = stream.collect(Collectors.toList());
Set<String> set = stream.collect(Collectors.toSet());

ArrayList<String> arrayList =
Stream.collect(Collectors.toCollection(ArrayList::new));
HashiSet<String> hashSet =
stream.collect(Collectors.eoCollection(HashiSet::new));

Map<Student, Double> studentToGPA =
	student.stream().collect(Collectors.toMap(Function.identity(),
			student -> computeGPA(student)));

Map<Boolean, List<Student>> passingFailing = students.stream()
	.collect(Collectors.partitioningBy(s -> s.getGrade() >=
	PASS_THRESHOLD));

Stream<String> stream = Stream.of("I","love","you");
String joined = stream.collect(Collectors.joining(",","{","}"));

int longest = 0;
for(string str : strings) {
	if(str.startsWith("A")) {
		int len = str.length();
		longest = Math.max(len,longest);
	}	
}

pblic final <R> Stream<R> map(Function<? super P_OUT, ? extends R>mapper){
	...
	return new StatelessOp<P_OUT>(this,StreamShape.REFERENCE,
			StreamOpFlag.Not_SORTED | StreamOpFlag.NOT_DISTINCT) {
		@Override
		Sink<P_OUT> opWrapSink(int flags, Sink<R> downstream){
			return new Sink.ChainedReference<P_OUT, R>(downstream) {
				@Override
				public void accept(P_OUT u) {
					R r = mapper.apply(u);
					downstream.accept(r);
				}
			};
		}
	};
}

}



