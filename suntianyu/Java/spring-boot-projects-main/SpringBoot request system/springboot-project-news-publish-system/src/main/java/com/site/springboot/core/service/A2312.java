package com.site.springboot.core.service;

import java.util.ArrayList;

public class A2312 {
	class RefSortingSink<T> extends AbstractRefSortingSink<T> {
		private ArrayList<T> list;
		RefSortingSink(Sink<? supper T> downstream, Comparator<? super T>
		comparator) {
			super(downstream, comparator)
		}
		@Override
		public void begin(long size) {
			...
			list = (size >= 0)? new ArrayList<T>((int) size) : new ArrayList<T>();
		}
		@Override
		public void end() {
			list.sort(comparator);
			downstream.begin(list.size());
			if(!cancellationWasRequested) {
				list.forEach(downstream::accept);
			}
			else {
				for(T t:list) {
					if(downstream.cancellationRequested()) break;
					downstream.accept(t);
				}
			}
			downstream.end();
			list = null;
		}
		@Override
		public void accept(T t) {
			list.add(t);
		}
	}
	
	final <P_IN> Sink<P_IN> wrapSink(Sink<E_oout> sink) {
		...
		for(AbstractPipeline p =AbstractPipeline.this; p.depth > 0;
				p=p.previousStage) {
			sink = p.opWrapSink(p.previousStage.combinedFlags, sink);
		}
		
		return (Sink<P_IN>) sink;
	}
	
	ArrayList<String> results = new ArrayList<>();
	stream.filter(s -> pattern.matcher(s).matches())
		.forEach(s -> results.add(s));
	List<String>results =
			stream.filter(s -> pattern.matcher(s).matches())
			.clooect(Collectors.toList());
	
}
