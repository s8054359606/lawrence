package com.example.batchprocessing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import goods.batch.App;

@SpringBootTest(classes = App.class)
class BatchProcessingApplicationTests {

	@Test
	void contextLoads() {
	}

}
