package goods.batch;

/**
 * *
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import goods.batch.entity.GoodsListEntity;
import goods.batch.service.GoodsService;

@Configuration
@EnableBatchProcessing
public class BatchApplication {

	@Autowired
	private GoodsService goodsService;

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Bean
	public Job ginzoJob() throws IOException {
		return this.jobBuilderFactory.get("ginzoJob").start(ginzoStep1()).build();
	}

	@Bean
	@Transactional
	public Step ginzoStep1() throws IOException {
		return this.stepBuilderFactory.get("ginzoStep1").tasklet(sampleTasklet1()).tasklet(sampleTasklet2()).build();
	}

	private Tasklet sampleTasklet1() throws IOException {
		Map<Integer, String> brand = new HashMap<Integer, String>();
		HashMapControl.putMap(brand);
		System.out.println("TASK1 START");
//		商品テーブルを削除する
		goodsService.delete();

		String homepage = "https://ginzo.jp/collections/%E3%83%96%E3%83%A9%E3%83%B3%E3%83%89%E3%82%B8%E3%83%A5%E3%82%A8%E3%83%AA%E3%83%BC";
		org.jsoup.nodes.Document doc0;
		int pagemax = 0;
		try {
			doc0 = Jsoup.connect(homepage).timeout(30000).get();
			Elements page = doc0.select("a.Pagination__NavItem.Link.Link--primary");
			String af = ".*page=.*";

			for (Element element : page) {
				String hre = element.attr("href");
				if (hre.matches(af) == true) {
					String page1 = page.text().replaceAll("<.*>.*</[\\w-\\W-]*>", "");
					String[] arr = page1.split("\\s");
					pagemax = Integer.parseInt(arr[arr.length - 1]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// page商品的鏈接を取得
		List<String> list = new ArrayList<String>();

//		for (int i = 1; i <= pagemax; i++) {
			for (int i = 1; i <= 5; i++) {
			String glURL = "https://ginzo.jp/collections/%E3%83%96%E3%83%A9%E3%83%B3%E3%83%89%E3%82%B8%E3%83%A5%E3%82%A8%E3%83%AA%E3%83%BC?page="
					+ i;
			System.out.println("------------------------------------" + i);
			Document doc1;
			try {
				doc1 = Jsoup.connect(glURL).timeout(30000).get();

				// srcを取得
				Elements select = doc1.select("div > h2 > a");
				String af1 = ".*collections/%E3%83%96%E3%83%A9%E3%83%B3%E3%83%89%E3%82%B8%E3%83%A5%E3%82%A8%E3%83%AA%E3%83%BC/products.*";
				list.clear();
				for (Element element : select) {

					String src = element.attr("href");

					if (src.matches(af1) == true) {
						// System.out.println("https://www.ippuukishi.co.jp/"+src);

						list.add("https://ginzo.jp" + src);
					}
//					System.out.println(list.size());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

//			for (int i1 = 0; i1 < list.size(); i1++) {
				for (int i1 = 0; i1 < 5; i1++) {
//				 具体画面
				String sURL = list.get(i1);

				Document doc2;
				doc2 = Jsoup.connect(sURL).timeout(30000).get();
				Elements button = doc2.select("button.ProductForm__AddToCart.Button.Button--secondary.Button--full");
				String dis = "Sold Out";
				for (Element element : button) {
					String action = element.text().replaceAll("<.*>.*</[\\w-\\W-]*>", "");
					if (action.matches(dis)) {
						System.out.println("已售出");
						continue;
					} else {
						// product Id
						Elements goodsId = doc2.getElementsByClass("ProductMeta__SkuNumber");

						// title
						Elements name = doc2.getElementsByTag("h1");
						System.out.println(name.text().replaceAll("・", " "));
						// intro
						Elements intro = doc2.select("div.Rte");
						List<String> intro1 = new ArrayList<String>();
						String styleRegex = "<style[^>]*?>[\\s\\S]*?<\\/style>";
						String tableRegex = "<table[^>]*?>[\\s\\S]*?<\\/table>";
						for (Element elements : intro) {
							intro1.add(elements.toString().replaceAll(styleRegex, "").replaceAll(tableRegex, "")
									.replaceAll("\\s*", "").replaceAll("・", ""));
						}
						System.out.println(intro1.get(0));

						// price
						Element price = doc2.select("span[data-money-convertible]").first();
						String[] strToInt = price.text().split(",");
						String tur = "";

						for (int i11 = 0; i11 < strToInt.length; i11++) {
							tur += strToInt[i11];
						}

						int intprice = Integer.parseInt(tur.replaceAll("¥", ""));

						// photo src
						List<String> listPhoto = new ArrayList<String>();
						Elements getImg = doc2.select("img[data-original-src]");// =doc.select("img")
						for (Element element11 : getImg) {
							String src = element11.attr("data-original-src");
							listPhoto.add("https:" + src);
						}

						int k;
						String photoall = "";
						for (k = 0; k < listPhoto.size(); k++) {
							photoall = photoall + "<img src=\"" + listPhoto.get(k) + "\" alt=\"\" />";
						}

						// details Table
						Elements getTable = doc2.getElementsByTag("table");
						List<String> deTable = new ArrayList<String>();
						for (Element element11 : getTable) {
							deTable.add(element11.toString());
						}
						System.out.println(deTable.get(0).replaceAll("<img.*src=(.*?)[^>]*?>", ""));

						GoodsListEntity goodsListEntity = new GoodsListEntity();
						// 商品名
						goodsListEntity.setTitles(name.text().replaceAll("・", " "));

						// ブランドネームinsert
						for (Integer key : brand.keySet()) {
							String str1 = name.text();
							int result1 = str1.indexOf(brand.get(key));
							if (result1 != -1) {
								goodsListEntity.setKeyword(brand.get(key));
								break;
							}
						}

						// 商品分類
						goodsListEntity.setCtype("cp");
						// 商品的標簽

						goodsListEntity.setContents("<p>" + "<style> \n" + "div{\n" + " font-size: 100%;\n"
								+ " line-height: 150%;\n}" + "table {\n" + " clear: both;\n" + "margin-bottom: 20px;\n"
								+ "width: 100%;" + "border-collapse: collapse;\n}\n" + "table td.td-ttl {\n"
								+ " background: #e5f4e5;\n" + " width: 4em;\n" + " color: #0c5727;\n }\n"
								+ "table td{\n" + " padding: 10px 8px 8px;\n" + " border: solid 1px #CCC;\n"
								+ " background-color: #FFF;\n }" + "\n</style>" + intro1.get(0)
								+ deTable.get(0).replaceAll("<img.*src=(.*?)[^>]*?>", "") + "\n" + "</p>" + "<p>"
								+ photoall + "</p>");

						// Appip Address
						String ipAdd = "";
						try {
							InetAddress ip4 = Inet4Address.getLocalHost();
							ipAdd = ip4.getHostAddress();

						} catch (UnknownHostException e) {
							e.printStackTrace();
						}
						goodsListEntity.setAppip(ipAdd);
						// Appuser 會員名
						goodsListEntity.setAppuser("15822223333");
						// Modtype
						goodsListEntity.setModtype("1");
						// enable 修改情況 1を設定
						goodsListEntity.setEnable("1");
						// accept 不明
						goodsListEntity.setAccept("0");
						// ctype_b（shopのidと一致する）
						goodsListEntity.setCtype_b(894);
						// city
						goodsListEntity.setCity(" ");
						// picname
						goodsListEntity.setPicname("");
						// price
						goodsListEntity.setPrice(intprice);
						// dlei int 大類
						goodsListEntity.setDlei(27);
						// xlei int 小類
						goodsListEntity.setXlei(0);

						List<GoodsListEntity> saveGoods = new ArrayList<>();
						saveGoods.add(goodsListEntity);
						goodsService.insertGoods(saveGoods);

					}
				}
			}

		}
		return null;

	}

	private Tasklet sampleTasklet2() {
		System.out.println("TASK2 START");

		return null;
	}
}