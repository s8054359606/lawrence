package goods.batch.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import goods.batch.entity.GoodsListEntity;

@Mapper
public interface GoodsMapper {

	int delete();

	int delete1();

	// 複数レコードをinsert
	int insertGoods(@Param("goodsList") List<GoodsListEntity> goodsList);

}
