let myname: string;
myname = "ebihara";
console.log(myname);

const array: string[] = [];
array.push("ebihara");
array.push("1");

function getName(id: string): string {
    // なんか処理
    return "hoge";
}
const name = getName("xxx");
console.log(name.length); // 4

function getName(id?: string): string {
    // id は、string か undefined の可能性がある。
    return "hoge";
}
const name = getName();
console.log(name); // 4




interface IUser {
    name: string;
    // １つの文字列の引数、戻り値が文字列の関数を定義している
    getName: (keisho: string) => string;
}

class User implements IUser {
    // name: string;
    name = "大森"
    // ここで、引数の型や数が違ったり、戻り値の型が違うと エラーとなる
    getName (keisho: string) {
        return `${this.name} (${keisho})`;
    }
}

const user = new User();
user.name = "ebiahra";
console.log(user.getName("さん"));
